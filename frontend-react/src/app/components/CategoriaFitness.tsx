export default function CategoriaFitness(){
    return(
        <div className="catfitness">
            <i className="fitnessicon ">
            </i>
            <h4>
            Body Building
            </h4>
            <p>
            Bodybuilding is the use of progressive resistance <br />
             exercise to control and develop one's muscles by <br />
              muscle hypertrophy for aesthetic purposes.
            </p>
        </div>
    )
}