export default function Button(props:{title:string; border?:boolean;icon?:boolean}){
    return(
        props.icon?
        <button className=" btn btn-border btn-icon">
            <i className="iconplay"></i>{props.title}</button>
        
        :
        <button className={props.border?'btn btn-border':' btn'}>{props.title}</button>
    )
}