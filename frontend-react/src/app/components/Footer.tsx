import Button from "./Button"
export default function Footer(){
    return (
        <h1>
            <Button title="Select Plan" border/>
            <Button title="Watch Reviews" border icon/>
            <Button title="Join Us"/>
        </h1>
    )
}