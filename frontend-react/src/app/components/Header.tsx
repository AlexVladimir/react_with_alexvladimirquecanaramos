import Button from "./Button";
import Bodyshape from "./Bodyshape";
export default function Header(){
    return(
        <div className="header">
            <nav>
            <Bodyshape></Bodyshape>
            <div className="links">
                <a href=""> Home</a>
                <a href="">Services</a>
                <a href="">About</a>
                <a href="">Contacts</a>
                <Button title="Join Us"/>

            </div>
            
            </nav>
            <div className="baner">

                <div className="content-baner">
                    <h1 className="baner-h1">
                        Transform <br />
                        Your Body
                    </h1>
                    <p className="baner-p">
                        We are dedicated to helping you transform your 
                        <br />
                        body and mind through the power of fitness.
                    </p>
                    <div className="baner-btn">
                        <Button title="Get Started"/>
                        <Button title="Watch Reviews" border icon/>
                    </div>
                </div>

            </div>

        </div>
        


    )
}