import Image from "next/image";
import styles from "./page.module.css";
import Plan from "./components/Plan";
import CategoriaFitness from "./components/CategoriaFitness";
export default function Home() {
  return (
    <main className={styles.main}>
      <div className="section">
        <h1  className="title">
          Fitness Plan & <span className="colortitle">Nutritions</span>
        </h1>
        <div className="categorias">
          
          <div className="item"><CategoriaFitness></CategoriaFitness></div>
          <div className="item"><CategoriaFitness></CategoriaFitness></div>
          <div className="item"><CategoriaFitness></CategoriaFitness></div>
          <div className="item"><CategoriaFitness></CategoriaFitness></div>
          <div className="item"><CategoriaFitness></CategoriaFitness></div>
          <div className="item"><CategoriaFitness></CategoriaFitness></div>

        </div>
        <h1  className="title">
          Suscribe To <span className="colortitle">Plans</span>
        </h1>
        <div  className="cajitas2">
          <Plan></Plan>
          <Plan></Plan>
          <Plan></Plan>
        </div>

      </div>
      
    </main>
  );
}
